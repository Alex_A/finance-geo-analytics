package ph.coins.model

import org.apache.spark.sql.types._

object AppSchema {
  val idColName        = "id"
  val ipColName        = "ip_address"
  val timestampColName = "timestamp"
  val amountColName    = "amount"

  private val idField         = StructField(idColName, StringType, nullable = false)
  private val ip_addressField = StructField(ipColName, StringType, nullable = false)
  private val timestampField  = StructField(timestampColName, TimestampType, nullable = false)
  private val amountField     = StructField(amountColName, DecimalType(38, 18), nullable = false)

  val readSchema = StructType(
    Array(
      idField,
      ip_addressField,
      timestampField,
      amountField
    )
  )
}
