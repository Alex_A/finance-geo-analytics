package ph.coins.app

import org.rogach.scallop.ScallopConf

/**
  * Main cli parsing class
  *
  * @param arguments The unparsed command line arguments
  */
class Conf(arguments: Seq[String]) extends ScallopConf(arguments) {
  appendDefaultToDescription = true
  val nodes = opt[String](
    descr = "Spark nodes (local run)",
    default = Option("local[*]")
  )
  val stay = opt[Boolean](
    descr = "Wait for key press to exit (to keep SparkSession and webserver running while debugging)",
    default = Option(false)
  )
  val input = opt[String](
    descr = "Path to the raw data to process (local, hdfs, s3)",
    required = true
  )
  val geoip = opt[String](
    descr = "Path to geoIp2 file (local, hdfs, s3)",
    required = true
  )
  val output = opt[String](
    descr = "Output path (local, hdfs, s3)",
    required = true
  )
  val limit =
    opt[Int](
      descr = "Limit DAG steps to given number, the read and write/show steps are always added"
    )
  val linesToShow = opt[Int](
    descr =
      "Amount of lines to shows to the console (instead of writing snappy compressed parquet files)"
  )
  val debug = opt[Boolean](
    descr = "Explains plan during DAG construction",
    default = Option(false)
  )

  verify()
}
