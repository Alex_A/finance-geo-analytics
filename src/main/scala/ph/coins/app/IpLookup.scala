package ph.coins.app

import com.snowplowanalytics.maxmind.iplookups.IpLookups
import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.spark.sql.functions.udf
import scalaz.{Failure, Success}

/**
  * This class provides functionality for country code IP lookups.
  *
  * The external dependency [[com.snowplowanalytics.maxmind.iplookups.IpLookups]]
  * is serialized to be passed to Spark context.
  *
  */
class IpLookup(geoIpDBPath: String) extends LazyLogging with Serializable {

  object SerializedLookups extends Serializable {
    val ipLookups = IpLookups(geoFile = Some(geoIpDBPath))
  }

  /**
    * Fetches country code from GeoLite2 DB by IP address.
    * @param ip IP address
    */
  def lookupCountry(ip: String): String =
    SerializedLookups
      .ipLookups
      .performLookups(ip)
      .ipLocation
      .flatMap {
        case Success(a) => Some(a.countryCode)
        case Failure(e) => logger.warn(f"${e.getMessage}"); None
      }.getOrElse("Unknown")

  def lookupCountryUDF: UserDefinedFunction = udf(lookupCountry _)

}
