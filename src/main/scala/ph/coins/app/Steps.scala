package ph.coins.app

import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.SparkFiles
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.apache.spark.sql.{Dataset, Row, SparkSession}
import ph.coins.model.AppSchema._

/**
  * This class contains the Steps to build a complete DAG
  *
  * Steps shall be written by using the [[org.apache.spark.sql.Dataset]] API,
  * functions from [[org.apache.spark.sql.functions]].
  *
  */
class Steps(spark: SparkSession, geoIPFile: String) extends LazyLogging {
  import Steps._
  import spark.implicits._

  private lazy val ipLookup = new IpLookup(SparkFiles.get(geoIPFile))

  /**
    * Read data from local / hdfs / s3 source, the first step of a DAG
    * @param input Path to the data to read
    */
  def read(input: String): Dataset[Row] =
    spark.read
      .schema(readSchema)
      .option("mergeSchema", "true")
      .parquet(input)

  def subsetColumns: DAGStep =
    _.select(col(idColName), col(ipColName), col(timestampColName), col(amountColName))

  def inferCountries: DAGStep =
    _.withColumn("country", ipLookup.lookupCountryUDF(col(ipColName)))

  def inferDates: DAGStep =
    _.withColumn("date", col(timestampColName).cast(DateType))
      .withColumn("month", month(col("date")))
      .withColumn("day", dayofmonth(col("date")))

  def countAvgDailySpendingByCountry: DAGStep =
    _.groupBy($"id", $"month", $"day", $"country")
      .agg(sum("amount").as("daily_amt"))
      .groupBy($"id", $"month", $"country")
      .agg(avg("daily_amt").as("avg_amount_per_day"))
      .select($"id", $"month", $"country", $"avg_amount_per_day")
      .orderBy($"id", $"month", $"country")

  /**
    * Write or show the data of a Dataset. This is the last step of a DAG.
    *
    * This step can do two things: write compressed parquet files or show a
    * specified number of lines. The last one is for developing / debugging.
    *
    * @param output The local / hdfs / s3 path to store the data
    * @param linesToShow Overwrites `parquet` if any value is given
    */
  def writeOrShowData(df: Dataset[Row], output: String, linesToShow: Option[Int]): Unit =
    if (linesToShow.isDefined)
      df.show(linesToShow.getOrElse(0), truncate = false)
    else
      df.write.mode("overwrite").parquet(output)
}

object Steps {
  type DAGStep = Dataset[Row] => Dataset[Row]
}
