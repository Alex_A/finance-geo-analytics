package ph.coins.app

import java.nio.file.Paths

import buildinfo.BuildInfo
import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.sql.{Dataset, Row, SparkSession}
import ph.coins.app.Steps.DAGStep

class Processing(spark: SparkSession, geoIPFile: String) extends LazyLogging {

  val steps = new Steps(spark, geoIPFile)

  val dagSteps: Vector[DAGStep] = Vector(
    steps.subsetColumns,
    steps.inferCountries,
    steps.inferDates,
    steps.countAvgDailySpendingByCountry
  )

  def process(input: String,
              output: String,
              limit: Option[Int],
              lines: Option[Int],
              debug: Boolean): Unit = {
    logger.info("Starting processing")

    val rawData = steps.read(input)
    logger.debug(s"The schema is now: ${rawData.schema.treeString}")

    val fullData = combineSteps(dagSteps, rawData, stepLimit = limit, debug)
    steps.writeOrShowData(fullData, output, linesToShow = lines)
    logger.info("Finished processing")
  }

  /**
    * Combines list of steps and print
    *
    * @param dagSteps The dag steps to combine
    * @param df The input Dataframe
    * @param stepLimit If set, Limits amount of combined steps to given number
    * @param debug If true, prints the physical and logical plan
    */
  def combineSteps(dagSteps: Vector[DAGStep],
                   df: Dataset[Row],
                   stepLimit: Option[Int],
                   debug: Boolean): Dataset[Row] =
    dagSteps
      .slice(0, stepLimit.getOrElse(dagSteps.size))
      .foldLeft(df)((dataframe, step) => {
        val rf = step(dataframe)
        logger.debug(s"The schema is now: ${rf.schema.treeString}")
        if (debug) rf.explain(true)
        rf
      })
}

object Processing extends LazyLogging {

  /**
    * Main entrypoint, parses the command line arguments, gets or creates the
    * SparkSession und starts the DAG processing.
    *
    * @param args The unparsed command line arguments
    */
  def main(args: Array[String]): Unit = {
    logger.info(s"Starting '${BuildInfo.name}' version '${BuildInfo.version}'")

    val conf = new Conf(args)
    logger.info(s"The command line parameters are: ${conf.summary}")

    lazy val spark = SparkSession.builder
      .master(conf.nodes())
      .appName(BuildInfo.name)
      .getOrCreate()

    spark.sparkContext.addFile(conf.geoip())

    val processing = new Processing(spark, Paths.get(conf.geoip()).getFileName.toString)
    try {
      processing.process(input = conf.input(),
                         output = conf.output(),
                         limit = conf.limit.toOption,
                         lines = conf.linesToShow.toOption,
                         debug = conf.debug())
    } finally {
      if (conf.stay()) {
        logger.info("Waiting: press enter to exit")
        logger.info(System.in.read().toString)
      }
      spark.stop()
    }
  }
}
