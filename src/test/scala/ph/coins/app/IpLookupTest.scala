package ph.coins.app

import java.nio.file.Paths

import org.scalatest.FlatSpec

class IpLookupTest extends FlatSpec {
  val geoIP    = Paths.get("data/GeoLite2-City.mmdb").toString
  val ipLookup = new IpLookup(geoIP)
  val testData: Seq[(String, String)] = Seq(
    ("5.182.197.0", "BN"),
    ("1.32.252.0", "KH"),
    ("43.254.56.0", "TL"),
    ("14.102.152.0", "ID"),
    ("5.253.184.0", "LA"),
    ("1.9.0.0", "MY"),
    ("34.98.230.0", "MM"),
    ("1.37.0.0", "PH"),
    ("1.37.1.0", "PH"),
    ("1.32.128.0", "SG"),
    ("1.0.128.0", "TH")
  )

  "ipLookup" should "correctly identify country IP" in {
    testData.foreach {
      case (given, expected) => assert(ipLookup.lookupCountry(given) === expected)
    }
  }

  it should "return `Unknown` for bad or unrecognized IP" in {
    assert(ipLookup.lookupCountry("0.0.0.0") === "Unknown")
  }

}
