
// https://github.com/sbt/sbt-buildinfo
addSbtPlugin("com.eed3si9n" % "sbt-buildinfo" % "0.9.0")

// Adds a `assembly` task to create a fat JAR with all of its dependencies
addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.9")

// disables warning: SLF4J: Failed to load class "org.slf4j.impl.StaticLoggerBinder".
// as explained here https://github.com/sbt/sbt-git
libraryDependencies += "org.slf4j" % "slf4j-nop" % "1.7.26"
