/*
 * Project metadata
 */

name := "SparkApp"

description := "Coding challenge solution for Data Engineer position."
organization := "ph.coins"
organizationName := "Coins.ph"
organizationHomepage := Some(url("https://coins.ph/"))

/*
 * scalac configuration
 */
// Use the same scala version Spark is build with, see scala.version in
// https://github.com/apache/spark/blob/master/pom.xml
scalaVersion in ThisBuild := "2.11.12"

compileOrder := CompileOrder.JavaThenScala

// Load test configuration and enable BuildInfo
lazy val root = Project("root", file("."))
  .configs(Testing.all: _*)
  .settings(Testing.settings)
  .enablePlugins(BuildInfoPlugin)
  .settings(
    buildInfoKeys := BuildInfoKey.ofN(name, version, scalaVersion, sbtVersion)
  )

// more memory Spark in local mode, see https://github.com/holdenk/spark-testing-base
javaOptions ++= Seq("-Xms512M", "-Xmx2048M", "-XX:+CMSClassUnloadingEnabled")

val commonScalacOptions = Seq(
  "-encoding", "UTF-8", // Specify character encoding used by source files
  "-target:jvm-1.8", // Target platform for object files
  "-Xfuture" // Turn on future language features
)

val compileScalacOptions = Seq(
  "-deprecation", // Emit warning and location for usages of deprecated APIs
  "-feature", // Emit warning and location for usages of features that should be imported explicitly
  "-g:vars",  // Set level of generated debugging info: none, source, line, vars, notailcalls
  "-unchecked", // Enable additional warnings where generated code depends on assumptions
  "-Xfatal-warnings", // Fail the compilation if there are any warnings
  "-Xlint:_", // Enable or disable specific warnings (see list below)
  "-Yno-adapted-args", // Do not adapt an argument list to match the receiver
  "-Ywarn-dead-code", // Warn when dead code is identified
  "-Ywarn-unused", // Warn when local and private vals, vars, defs, and types are unused
  "-Ywarn-unused-import", // Warn when imports are unused
  "-Ywarn-value-discard" // Warn when non-Unit expression results are unused
)

scalacOptions ++= commonScalacOptions ++ compileScalacOptions ++ Seq(
  "-Ywarn-value-discard" // Warn when non-Unit expression results are unused
)

scalacOptions in (Test, compile) := commonScalacOptions ++ compileScalacOptions


scalacOptions in (Test, console) := (scalacOptions in (Compile, console)).value

// Have fullClasspath during compile, test and run, but don't assemble what is marked provided
// https://github.com/sbt/sbt-assembly#-provided-configuration
run in Compile := Defaults
  .runTask(fullClasspath in Compile, mainClass in (Compile, run), runner in (Compile, run))
  .evaluated

/*
 * Managed dependencies
 */
val sparkVersion           = "2.4.3"
val clusterDependencyScope = "provided"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % sparkVersion,
  "org.apache.spark" %% "spark-sql"  % sparkVersion,
  "com.snowplowanalytics" %% "scala-maxmind-iplookups" % "0.4.0",
  "org.slf4j"                  % "slf4j-log4j12"  % "1.7.26",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
  "org.rogach"                 %% "scallop"       % "3.3.1"
).map(_.exclude("ch.qos.logback", "logback-classic"))

libraryDependencies ++= Seq(
  "org.scalatest"    %% "scalatest"          % "3.0.8"
) map (_ % Test)

/*
 * sbt options
 */
// Add task to check for sbt plugin updates
addCommandAlias("pluginUpdates", "; reload plugins; dependencyUpdates; reload return")

// Do not exit sbt when Ctrl-C is used to stop a running app
cancelable in Global := true

// Improved dependency management
updateOptions := updateOptions.value.withCachedResolution(true)

showSuccess := true
showTiming := true

// Enable colors in Scala console (2.11.4+)
initialize ~= { _ =>
  val ansi = System.getProperty("sbt.log.noformat", "false") != "true"
  if (ansi) System.setProperty("scala.color", "true")
}

// Draw a separator between triggered runs (e.g, ~test)
triggeredMessage := { ws =>
  if (ws.count > 1) {
    val ls = System.lineSeparator * 2
    ls + "#" * 100 + ls
  } else { "" }
}

shellPrompt := { state =>
  import scala.Console.{ BLUE, BOLD, RESET }
  s"$BLUE$BOLD${name.value}$RESET $BOLD\u25b6$RESET "
}

/*
 * sbt-assembly https://github.com/sbt/sbt-assembly
 */
test in assembly := {}
// scala-library is provided by spark cluster execution environment
assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false)
assemblyShadeRules in assembly := Seq(
  ShadeRule.rename("com.fasterxml.**" -> "shadeio.@1").inAll
)
assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}
