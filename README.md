# Coins Data Engineering Challenge
## "Finance-geo analytics"
### Context
Coins.asia is the digital wallet company with millions of real customers.
Our customers are able to make payments with their phone on daily basis.
We operate in SEA countries (Thailand, Philippines, Singapore, ..).
Business development department decides they want to have geo analytics for financial
transactions.

Our financial transactions log has the following fields:
* id (user id, type: string, example: "b4e5cb45-92d5-417e-8ed7-8c991997d256")
* ip_address (transaction ip address, type: string, example: "175.176.17.74")
* timestamp (unix timestamp in seconds, type: integer, example: 1559736152)
* amount (amount of money spent in dollars, type: decimal with precision up to 38 digits and scale up to 18 digits, example: 1649.1842348723424)
Data is stored in parquet files with daily partitioning.

### Task
Write the spark application that calculates for each user the average amount of money spent by user per day in each country on monthly basis.
The time spent in each country can be calculated based on the timestamps.
Time between transactions done from the same country can be considered as the time spent by the user in the country.
To solve the task you need to find the way to determine the country where the transaction was made.

The result dataset should have the following columns:
* id
* month
* country
* avg_amount_per_day

### Bonus task
Suggest the architecture for the production grade solution assuming the following
requirements:
* data in the prepared report will be used by other important derived ETL processes
* data in the prepared report we be used to build the dashboard that is updated on daily basis

### Requirements
* Apache Spark
* Implementation language - Scala / Java at your choice
* Build tool at your choice
* Readme describing implementation, how to build and run the application
* Basic Unit Test coverage

## Solution
### Overview
The project is implemented in [Scala v2.11.12](https://www.scala-lang.org/) 
with use of [Scala Build Tool v1.2.8](https://www.scala-sbt.org/) 
and [Apache Spark v2.4.3](https://spark.apache.org/).

### Solution considerations
- It is assumed that since parquet file has been `partitioned by day` means, that when parsed as dataframe, it has `day`, `month` and `year` columns after applying `mergeSchema=true` option in read step.
- Also it is assumed that only `timestamp` is representative of the transaction date. Thus `timestamp` would be used to produce new `day` and `month` columns.
- Since a combination of IP address and timestamp suggests, that a person has performed the transaction at a specific time and a specific country, 
it is assumed that no additional data transformation is required besides grouping by month, day and country so get the total daily spending of a person at a given country.
- `ISO 3166 Country Code` is selected to represent country, as given by `Country by IP` functionality provider. In real-life setting, the consumer of the data may require certain standard encoding, e.g. one of `ISO 3166` versions or even `FIPS 10-4`, depending on jurisdiction.

### Implementation
The implementation is based on [Spark Scala Template](https://github.com/dbast/spark-scala-template), created by Marconi Lanna and Daniel Bast.

It offers 3 layers:
* The reusable shared DAG steps, implemented via (SQL like) Dataset API;
* The user defined functions used by the steps;
* A command with cli, that builds a whole DAG by combining steps.

User defined functions are replaced with `IpLookup` class, which relies on (Scala MaxMind IP Lookups)[https://github.com/snowplow/scala-maxmind-iplookups] Scala library.
Also, please keep in mind that most of the functionality and Scala goodies were stripped off the template.
While stock facilities are indeed useful in production environment, the solution reqiured only few of them.

The transformation DAG consists of the following steps:
* `subsetColumns` - it selects `id`, `ip`, `timestamp` and `amount` columns from the dataframe;
* `inferCountries` - this steps looks up for a country code by IP;
* `inferDates` - the `timestamp` is transformed to date here, then transaction `month` and `day` are inferred;
* `countAvgDailySpendingByCountry` - the core aggregations happen here.

The app consumes `parquet` dataset, partitioned by days and outputs either to console or to some other `parquet` file.
Basic logging functionality has been provided along with a unit test for IP Lookups.

### Country IP Lookup 
The country lookup by IP functionality is provided by (Scala MaxMind IP Lookups)[https://github.com/snowplow/scala-maxmind-iplookups] library 
and (GeoLite2)[https://dev.maxmind.com/geoip/geoip2/geolite2/] database.

### Run Requirements
In order to run the application a [GeoLite2 City MaxMind DB binary, gzipped](https://geolite.maxmind.com/download/geoip/database/GeoLite2-City.tar.gz) should be downloaded, 
unpacked and put under `data/` directory.

### Included assets
`testDataset.parquet` dataset had been included in project assets. It has been generated according to data schema, provided in challenge. 
The partition was generated, based on timestamp (while in real life it might not be the case). 
Dataset count is 10000 records. It includes random-amount transactions between `January 01 2019 00:00` and `July 01 2019 00:00` with IPs for the following countries:

### How to build
In order to build the `SparkApp` please enter `sbt` console and then call `clean`, `compile` and finally `assembly` commands. 
A FAT JAR would be generated to `/target/scala-2.11/SparkApp-assembly-<version>-SNAPSHOT.jar`.

### How to run
`SparkApp` has the following run arguments:
```
-d, --debug                  Explains plan during DAG construction
-i, --input  <arg>           Path to the raw data to process (local, hdfs, s3)
    --geoip  <arg>           Path to geoIp2 file (local, hdfs, s3)
-l, --limit  <arg>           Limit DAG steps to given number, the read and
                             write/show steps are always added
    --lines-to-show  <arg>   Amount of lines to shows to the console (instead
                             of writing snappy compressed parquet files)
-n, --nodes  <arg>           Spark nodes (local run) (default = local[*])
-o, --output  <arg>          Output path (local, hdfs, s3)
-s, --stay                   Wait for key press to exit (to keep SparkSession
                             and webserver running while debugging)
    --help                   Show help message
```

Before running the app, please make sure there's a [GeoLite2 City MaxMind DB binary](https://dev.maxmind.com/geoip/geoip2/geolite2/) in `/data` folder .

Please consider the following command:
```
/path/to/spark-home/bin/spark-submit \
--class ph.coins.app.Processing \
--name <awesome app name> \
--master <master url> \
./target/scala-2.11/<jar name> -i data/testDataset.parquet --geoip data/GeoLite2-City.mmdb -o test -d --limit 4 --lines-to-show 20 
```


- It submits a Spark Job with `<awesome app name>` to `<master url>`, by providing its JAR `<jar name>` to Spark.  
- The run parameters mean that `data/testDataset.parquet` would be used as input and `data/GeoLite2-City.mmdb` would be used as a source of `IP to Country code` mapping.
- The resulting dataset would be saved to `test` folder.
- `-d` will turn on the debug mode.
- The depth of evaluation is limited to `4` steps of the entire DAG.
- Only `20` lines of the result would be shown. Presence of this parameter overrides the output of the file to `test` folder.

### Output example
See output example below. Please bear in mind, that data is synthetic (i.e. generated randomly) and is not representative of real transactions.

```
+------------------------------------+-----+-------+---------------------------+
|id                                  |month|country|avg_amount_per_day         |
+------------------------------------+-----+-------+---------------------------+
|050c8274-2457-482a-9dd4-bf469834a706|1    |BN     |1328.1507440312027223200000|
|050c8274-2457-482a-9dd4-bf469834a706|1    |ID     |1521.6990308434624083300000|
|050c8274-2457-482a-9dd4-bf469834a706|1    |KH     |1530.4016468276751937900000|
|050c8274-2457-482a-9dd4-bf469834a706|2    |LA     |1221.1224771827494904300000|
|050c8274-2457-482a-9dd4-bf469834a706|2    |MM     |1080.7158112554864859700000|
|050c8274-2457-482a-9dd4-bf469834a706|3    |MY     |1404.7091520207169983000000|
|050c8274-2457-482a-9dd4-bf469834a706|3    |PH     |2466.0318810464345719200000|
|050c8274-2457-482a-9dd4-bf469834a706|4    |SG     |1255.8954112431351766900000|
|050c8274-2457-482a-9dd4-bf469834a706|4    |TH     |1697.2405866046447165900000|
|13c2711c-022d-419a-92ca-8dc84eb70524|1    |MY     |1340.5592106142223611200000|
|1645e924-af19-4c69-91b8-ee50ae8c9bf6|1    |BN     |1670.7781488258040947700000|
|4d6d4062-7a98-4f7d-8b58-6643a1dd9e58|1    |TL     |1447.0656002178718632000000|
|d9b2f9a5-a03d-49a3-848b-e5c3ff706bdb|1    |TL     |1330.6680843968009198300000|
|4d6d4062-7a98-4f7d-8b58-6643a1dd9e58|1    |MM     |1117.4507019813207939200000|
|13c2711c-022d-419a-92ca-8dc84eb70524|1    |LA     |1590.3399667028745737000000|
+------------------------------------+-----+-------+---------------------------+
```

### Continuous Integration
`.gitlab-ci.yml` with minimal integration steps has been included into the project to enable CI/CD practices.

### Further Support
Further support requires regular updating of `GeoLite2-City.mmdb` file. 
For details, please visit [GeoIP2 website](https://dev.maxmind.com/geoip/geoip2/geolite2/).

## Enhancement Suggestions
### Architecture suggestion on derived ETL processes
`parquet` files offer cost efficient storage functionality, lightweight and fast IO. They may be used in couple with a data storage of choice.
Output data must be saved in such files in accordance with the data needs of organisation. For example, 
given the understanding of the required data flow between producers and consumers of transaction data, it may be required to extend the app functionality with additional transformations.

### Architecture suggestion on daily-updated dashboards
This might require integration with additional persistence layer and dashboard service like Grafana, which supports following data sources: 
* Clickhouse
* Prometheus
* Elasticsearch
* Google Stackdriver
* AWS Cloudwatch
* Azure Monitor
* etc.

The developed SparkApp should be extended with a suitable DB integration functionality and should be triggered in a periodic fashion to upload the data into the DB.
Dashboards could be developed in the app of preference and set to refresh at a desired interval.

### Other improvements
The solution may definitely benefit from the rewiring of the `countAvgDailySpendingByCountry` DAG step to `ReduceByKey` implementation, which is more efficient than `GroupBy`. 

## Other Considerations
### Motivation on use of templates
Use of templates enables rapid in development, cost-effective and production ready solutions. 
Also, by providing additional machinery, it allows developers, who will support and extend existing functionality, to effectively shorten the ‘code—>see result’ feedback loop.

The following templates have been considered:
* `holdenk` [sparkProjectTemplate](https://github.com/holdenk/sparkProjectTemplate.g8)
* `dbast` [Spark Scala Template](https://github.com/dbast/spark-scala-template)

The former template by Holden Karau offers basic Spark App functionality. 
Unfortunately, it does not offer integration testing facilities like the latter template. With latest commit made on Aug 06, 2018 the template seems a bit outdated.

The latter by Daniel Bast offers an assortment of default settings, best practices, and general goodies for Scala projects. It is actively supported with the latest commit made on Jun 14, 2019. In addition to testing, code coverage, packaging, and CI facilities it features:
* extended CLI options
* DAG inspection for debugging 
* Versioning support
* Scala REPL with common imports
* Scala Style support
* sbt-updates
* License reports
* dependency tree display
* etc.

By sheer amount of features and active support `Spark Scala Template` goes as a template of choice.

### A note on dependency shading and provisions
Please note, that `com.fasterxml.**` libraries have been shaded with `sbt-assembly` in order to resolve dependency conflict between Spark libraries and `scala-maxmind-iplookups`.
Also, for the real-life situation, a `provided` tag might be required for `spark-core` and `spark-sql` libraries.










